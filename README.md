# freva workshop 2024.06.12-13

In this repository you can find all the necessary information and links related to the [Freva workshop for users and plugin developers](https://events.dkrz.de/event/65/).

For any problem or suggestion, please do not hesitate to contact us at freva@dkrz.de

## 1. General Usage (day 1)

- Introductory talk: [here](https://freva-clint.github.io/Talks/Freva_Workshop_2024/day%201/index.slides.html#/)
- Exercises, 2 jupyter notebooks:
    - freva command line interface (cli):  `./hands-on/freva_users_course_cli.ipynb` (if you clone the repo) or [here](https://gitlab.dkrz.de/freva/freva_workshop/-/blob/main/hands-on/freva_users_course_cli.ipynb?ref_type=heads). It needs the **freva bash kernel** installed (instructions in the notebook itself)
    - freva python: `./hands-on/freva_users_course_py.ipynb` (if you clone the repo) or [here](https://gitlab.dkrz.de/freva/freva_workshop/-/blob/main/hands-on/freva_users_course_py.ipynb?ref_type=heads). It needs the **freva python kernel** installed (instructions in the notebook itself)
- Data: `./data/my_precip_data.nc` (if you clone the repo) or [here](https://gitlab.dkrz.de/freva/freva_workshop/-/blob/main/data/my_precip_data.nc?ref_type=heads)
- Video recordings for day 1 are [here](https://swiftbrowser.dkrz.de/tcl_objects/2029-06-27T11:08:29Z/r_00d9e85f2e461aa4f3b5ea2f46e1d62cc4c4c5e4/w_/dkrz_d0859f7c-0da8-41c7-824d-58b3765f5ec4/freva_workshop_202406/36/day1_freva-general-usage_recordings/)

## 2. Plugin Development (day 2)

- Plugin developers talks (all in one): [here](https://freva-clint.github.io/Talks/Freva_Workshop_2024/day%202/index.slides.html#/)
- Exercises, 1 jupyter notebook:
    - freva plugin:  `./hands-on/freva_plugins_course.ipynb` (if you clone the repo) or [here](https://gitlab.dkrz.de/freva/freva_workshop/-/blob/main/hands-on/freva_plugins_course.ipynb?ref_type=heads). It needs the **freva bash kernel** installed (instructions in the notebook itself)
- Plugin: you will need to clone it from [here](https://gitlab.dkrz.de/freva/plugins4freva/plugin_workshop/example_plugin)
- Video recordings for day 2 are [here](https://swiftbrowser.dkrz.de/tcl_s/16qQ4SXEo8x2cU)